Deputy
------

The Deputy module helps developers and site-builders "deputize" users of their
application with a limited set of configuration control. It is not a permissions-
management module.

## Okay, so what is it?

Deputy's primary focus is on two things:

  * Helping developers encapsulate functionality in such a way that it can be
    easily enabled or disabled via a simple variable.
  * Building a clean, centralized UI for those bits of functionality so someone
    with limited site privileges can still find a central administrative page
    to control those pieces.

Does that sound familiar? It's somewhat inspired in concept by the Features
module. The difference here is that Features is primarily focused on
exportability, with it's "delegation" functionality being a secondary affect of
using Features alongside modules like Spaces and OG Features.

Deputy reverses that relationship, focusing on the restricted functionality (on
a site-wide basis) and secondarily allowing developers to create code inside
the managed file.

## How does it work?

Implement hook_deputy_default_systems() and you can define pseudo-module
namespaces. For a number of pre-determined hooks in the Deputy module, deputy
will "handoff" it's own implementation to also have an implementation under any
Deputy System's namespace.

Deputy also allows for the definition of an include file to hold that code, and
will include the file as needed.

## What else can you do with it?

Well, Deputy auto-builds a UI at admin/settings/deputy, and this UI will even
include an optional fieldset for each Deputy System that cares to define and
implement a 'settings callback' function (which should be Form array).

On top of that, it makes an excellent, specialized place to drop any application-
specific configuration stuff.
