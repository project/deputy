<?php
/**
 * @file
 * Define pages for the Deputy module.
 */

/**
 * Form callback to toggle on and off Deputy Systems. 
 */
function deputy_settings_form() {
  deputy_include_enabled();

  $form = array(
    'description' => array(
      '#value' => t('When saving new settings there will be a delay for cache clearing.') . '<br/><br/>',
    ),
  );

  $form['deputy_systems'] = array(
    '#tree' => TRUE,
  );

  foreach (deputy_system_load() as $name => $system) {
    if (empty($system['final'])) {
      $form['deputy_systems'][$name] = array(
        '#title' => $system['title'],
        '#description' => $system['description'],
        '#type' => 'checkbox',
        '#access' => deputy_admin_access($name),
        '#default_value' => deputy_system_exists($name),
      );

      if ($form['deputy_systems'][$name]['#default_value'] && !empty($system['configure'])) {
        $menu = menu_get_item($system['configure']);
        if ($menu['access']) {
          $form['deputy_systems'][$name]['#description'] .= ' ' . l(t('Configure'), $system['configure']);
        }
      }
    }
  }

  foreach (deputy_system_list() as $name => $system) {
    if (!empty($system['settings callback']) && function_exists($system['settings callback'])) {
      $fieldset_key = 'deputy_settings_' . $name;
      $form[$fieldset_key] = array(
        '#type' => 'fieldset',
        '#title' => $system['title'],
        '#description' => $system['description'],
        '#access' => deputy_admin_access($name),
      );
      // Any additional options for the fieldset should be at the root of the
      // settings callback return value. Good place for #access, #tree, etc.
      $form[$fieldset_key] = array_merge($form[$fieldset_key], $system['settings callback']());
    }
  }

  // Handle deputy systems, then remove before system_settings_form_submit can touch it.
  $form['#submit'][] = 'deputy_system_status_submit';
  // Add standard handling.
  $form = system_settings_form($form);
  // Add a massive cache clear as needed.
  $form['#submit'][] = 'deputy_settings_cache_clear_submit';
  return $form;
}

/**
 * Submit callback to handle activation/deactivation of Deputy systems.
 */
function deputy_system_status_submit($form, &$form_state) {
  foreach ($form_state['values']['deputy_systems'] as $system => $enable) {
    if (deputy_system_exists($system) != $enable) {
      $form_state['deputy_cache_clear'] = TRUE;
      if ($enable) {
        deputy_system_enable($system);
      }
      else {
        deputy_system_disable($system);
      }
    }
  }
  unset($form_state['values']['deputy_systems']);
}

/**
 * Clears drupal caches as needed if other handlers detect signficant change.
 *
 * This should be the last submit handler, or at least the last after the primary
 * deputy submit callbacks.
 */
function deputy_settings_cache_clear_submit($form, &$form_state) {
  if (!empty($form_state['deputy_cache_clear'])) {
    variable_set('perm_conf_update_needed', 1);
    drupal_flush_all_caches();
  }
}
