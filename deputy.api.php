<?php
/**
 * @file
 * API documentation for Deputy module.
 */

/**
 * Defines a system that can be deputized to a user with the right permission.
 *
 * The definition of a system is intended to create a combination of a simple
 * "module-lite" include file that has super clean integration with a central
 * site configuration console suitable for exposing to minimally privileged
 * site managers.
 *
 * Those site manager "Deputies" are able to control systems via Deputy config
 * that they normally don't have the power to touch. In this way you insulate
 * this class of users from the deeper settings of the site, while still
 * empowering them with the kinds of controls you do want to expose. Even so,
 * every Deputy System is automatically paired with a permission, meaning a
 * given role might handle all systems (the "sheriff" permission) or a specific
 * subset.
 *
 * The return value for this hook is entirely made up of optional elements. By
 * simply defining an empty array associated with a machine name as the key,
 * a toggleable system will be created. When activated, it's namespace will be
 * targeted by Deputy for a second-order hook invocation of a selection of hooks
 * that are individually cooked in to Deputy. More will be added as needed.
 *
 * @return
 *   Array of Deputy definitions, each an array keyed off the system's intended
 *   machine name.
 *   - title: (Optional) For human-readable labels, such as the system's fieldset.
 *   - description: (Optional) For longer-winded descriptions of the system.
 *   - path: (Optional) Path to a Deputy System "control" file. A pseudo-module that can
 *     carry implementations of predefined hooks. If you need multiple files for
 *     this, the "main" file can handle followup inclusion.
 *   - configure: (Optional) If set, will be appended to option description for privileged users.
 *   - settings callback: (Optional) Form to include with expanded settings for
 *     the given Deputy system.
 *   - status: (Optional) If TRUE, will default the system to ENABLED. Note that
 *     changing the status in code after the Deputy Settings Form has been saved
 *     will not do anything, it only facilitates initial defaults, or the status
 *     of 'final = TRUE' systems that have not been programmatically overridden.
 *   - final: (Optional) If TRUE, will not provide UI to change the system status.
 */
function hook_deputy_default_systems() {
  $items = array();

  $items['blog'] = array(
    'title' => t('Blogging'),
    'description' => t('A blogging system to facilitate user soapboxing.'),
    'path' => drupal_get_path('module', 'deputized_features') . '/includes/blog.inc',
    'configure' => 'admin/settings/blog',
    'settings callback' => 'blog_settings_form',
  );

  return $items;
}

/**
 * Modify the Deputy system definitions pre-cache.
 */
function hook_deputy_default_systems_alter(&$items) {
  $items['blog']['settings callback'] = 'better_blog_settings_form';
}

/**
 * Modify the Deputy system definitions per page load.
 */
function hook_deputy_systems_load_alter(&$items) {
  // Adjust the human-readable name of the system to use the current Group title.
  $node = og_get_group_context();
  $items['blog']['title'] = t('%title blog', array('%title' => $node->title));
}
