<?php
/**
 * @file
 * Library functionality for the Deputy module.
 */

/**
 * Determine whether a given deputy include is active.
 *
 * @param $system
 *   The name of the Deputy system to check.
 * @param $default
 *   Default the status of a deputy system if not otherwise set. Default FALSE.
 */
function deputy_system_exists($system) {
  $list = deputy_system_list();
  return array_key_exists($system, $list);
}

/**
 * Collect a list of loaded Deputy systems.
 *
 * @param $reset
 *   If TRUE, rebuild the list of enabled systems.
 *
 * @note
 *   This should not be used for individual disable/enable commands because
 *   there are often multiple status changes as part of a single page operation.
 * @todo
 *   There is no concept of weight or sorting here. Probably worth adding.
 */
function deputy_system_list($reset = FALSE) {
  static $list;
  
  if (!isset($list) || $reset) {
    $list = array();
    foreach (deputy_system_load() as $name => $system) {
      // This is where it might check whether the system path was actually included.
      // Because the simple array operation against *all files included by php*
      // would be a performance hit, we are not doing it. If we wanted to, we would
      // separately store deputy_include() actions and check it here.
      $status = isset($system['status']) ? $system['status'] : FALSE;
      if (variable_get('deputy_enabled_system_' . $name, $status)) {
        $system['status'] = TRUE;
        $list[$name] = $system;
      }
    }
  }

  return $list;
}

/**
 * Mark a given deputy system as enabled.
 *
 * @param $system
 *   The name of the Deputy system to check.
 */
function deputy_system_enable($system) {
  variable_set('deputy_enabled_system_' . $system, TRUE);
  deputy_invoke($system, 'enable');
}

/**
 * Mark a given deputy system as disabled.
 *
 * @param $system
 *   The name of the Deputy system to check.
 */
function deputy_system_disable($system) {
  variable_set('deputy_enabled_system_' . $system, FALSE);
  deputy_invoke($system, 'disable');
}
